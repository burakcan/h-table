# Hierarchy Table Example

## Requirements

To run this project, you will need [Elm](http://elm-lang.org), [Node.js](http://nodejs.org/) >= 6.0.0. NPM(comes bundled with Node.js) installed on your environment.


## Install

Clone the project and install dependencies:

    $ git clone https://gitlab.com/burakcan/h-table.git
    $ cd h-table
    $ npm install
    $ elm-package install

## Start & watch

    $ npm run start-dev

Now you can browse the app at http://localhost:4000

## Build for production

    $ npm run build
