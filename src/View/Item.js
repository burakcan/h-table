import React from 'react';
import Table from './Table';
import styles from './style.css';

function multiReturn(props) {
  return props;
}

export default function Item({ person, columns, path, onExpand, onDelete }) {
  const hasChildren = (
    (person['has_phone'] && person['has_phone'].length) ||
    (person['has_relatives'] && person['has_relatives'].length)
  );

  return (
    <tbody className={ styles.Item }>
      <tr>
        <td className={ styles.Expand }>
          <button
            className={ hasChildren && person.expanded ? styles.expanded : '' }
            onClick={ () => onExpand(path) }
            disabled={ !hasChildren }
          >
            <i className="material-icons">chevron_right</i>
          </button>
        </td>
        <td className={ styles.Delete }>
          <button
            onClick={ () => onDelete(path) }
          >
            <i className="material-icons">delete_forever</i>
          </button>
        </td>
        { columns.map(key =>
          <td
            key={ key }
            className={ styles.ItemColumn }
          >
            { person.data[key] || '-' }
          </td>
        )}
      </tr>
      { person.expanded && ['has_relatives', 'has_phone'].map((key, i) =>
        (person[key] && person[key].length) ? (
          <tr key={ key } className={ styles.Child }>
            <td colSpan={ columns.length + 2 }>
              <Table
                personList={ person[key] }
                path={ path.concat(key) }
                onExpand={ onExpand }
                onDelete={ onDelete }
              />
            </td>
          </tr>
        ) : null
      )}
    </tbody>
  );
}
