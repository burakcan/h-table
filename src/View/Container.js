import React from 'react';
import connectElmStore from './connectElmStore';
import Table from './Table';

function confirmDelete(path, send) {
  const confirmed = window.confirm(
    'Are you sure you want to delete this item and all it\'s children?'
  );

  if (confirmed) send(path);
}

function mapStateToProps(state) {
  return {
    personList: state.personList || [],
  };
}

function mapPortsToProps(ports) {
  return {
    onExpand: ports.onExpandPort.send,
    onDelete: path => confirmDelete(path, ports.onDeletePort.send),
  };
}

export default connectElmStore(mapStateToProps, mapPortsToProps)(
  props => (
    <Table
      personList={ props.personList }
      onExpand={ props.onExpand }
      onDelete={ props.onDelete }
    />
  )
);
