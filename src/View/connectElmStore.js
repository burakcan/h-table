import React, { Component } from 'react';

export default
  (mapStateToProps, mapPortsToProps) =>
    Komponent => {
      class Connect extends Component {
        constructor(props, context) {
          super(props, context);

          this.state = {};

          props.store.ports.modelDumpPort.subscribe(
            this.handleModelDump.bind(this)
          );
        }

        handleModelDump(payload) {
          if (this._lastDump === payload) {
            return false;
          }

          this._lastDump = payload;

          return this.setState(
            JSON.parse(payload)
          );
        }

        render() {
          return (
            <Komponent
              { ...this.props }
              { ...mapPortsToProps(this.props.store.ports) }
              { ...mapStateToProps(this.state) }
            />
          );
        }
      }

      return Connect;
    }
