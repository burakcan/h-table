import React from 'react';
import Item from './Item';
import styles from './style.css';

function getColumnKeys(personList) {
  return personList.reduce((result, { data }) => {
    const newResult = [...result];

    Object.keys(data).forEach(key => {
      if (!newResult.includes(key)) {
        newResult.push(key);
      }
    });

    return newResult;
  }, []);
}

export default function Table({ personList, path = [], onExpand, onDelete }) {
  const columns = getColumnKeys(personList);

  return (
    <div className={ styles.TableWrapper }>
      { personList.length === 0 ? (
        <div className={ styles.EmptyView }>
          <i className="material-icons">sentiment_very_dissatisfied</i>
          Why did you do that?
        </div>
      ) : (
        <table className={ styles.Table }>
          <thead>
            <tr>
              <th></th>
              <th></th>
              { columns.map(key => <th key={ key }>{ key }</th>) }
            </tr>
          </thead>
            { personList.map((person, i) =>
              <Item
                key={ path.concat(i).toString() }
                path={ path.concat(String(i)) }
                person={ person }
                columns={ columns }
                onExpand={ onExpand }
                onDelete={ onDelete }
              />
            )}
        </table>
      )}
    </div>
  );
}
