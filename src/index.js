import 'normalize.css';
import React from 'react';
import { render } from 'react-dom';
import data from './data.json';
import { Store } from './Store.elm';
import Container from './View/Container';

const store = Store.worker({ personList: data });

render(
  <Container store={ store } />,
  document.getElementById('root')
);
