module Store exposing (..)

import Store.State exposing (init, update, subscriptions)
import Html exposing (text)
import Html.App exposing (programWithFlags)


main =
    programWithFlags
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = (\_ -> text "")
        }
