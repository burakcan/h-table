port module Store.Border exposing (modelDumpPort, onExpandPort, onDeletePort)

import Json.Decode


port modelDumpPort : String -> Cmd msg


port onExpandPort : (List String -> msg) -> Sub msg


port onDeletePort : (List String -> msg) -> Sub msg
