module Store.Types exposing (..)

import Dict exposing (Dict)
import Array exposing (Array)


type PersonList
    = PersonList (Array Person)


type alias Person =
    { data : Dict String String
    , expanded : Bool
    , has_relatives : Maybe PersonList
    , has_phone : Maybe PersonList
    }


type alias Flags =
    { personList : String }


type Msg
    = OnExpand (List String)
    | OnDelete (List String)


type alias Model =
    { personList : Maybe PersonList
    }
