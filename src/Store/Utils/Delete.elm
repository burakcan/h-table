module Store.Utils.Delete exposing (delete)

import Store.Types exposing (PersonList(..))
import Result
import Array exposing (Array)
import String


delete : List String -> PersonList -> PersonList
delete path (PersonList personList) =
    -- Recursively walk in the list by following the path
    -- Btw elm sucks at nesting :)
    Array.indexedMap (\i person -> ( i, person )) personList
        |> filterMap
            (\( i, item ) ->
                let
                    pathHead =
                        List.head path

                    isMe =
                        Maybe.withDefault False <|
                            Maybe.map
                                (\head ->
                                    i == Result.withDefault -i (String.toInt head)
                                )
                                pathHead

                    nextItem =
                        if List.length path == 1 && isMe then
                            Nothing
                        else if isMe then
                            Maybe.map
                                (\head ->
                                    if head == "has_relatives" then
                                        { item
                                            | has_relatives =
                                                Maybe.map
                                                    (delete <| List.drop 2 path)
                                                    item.has_relatives
                                        }
                                    else if head == "has_phone" then
                                        { item
                                            | has_phone =
                                                Maybe.map
                                                    (delete <| List.drop 2 path)
                                                    item.has_phone
                                        }
                                    else
                                        item
                                )
                            <|
                                List.head (List.drop 1 path)
                        else
                            Just item
                in
                    nextItem
            )
        |> PersonList


filterMap : (a -> Maybe b) -> Array a -> Array b
filterMap f xs =
    -- from Array.Extra
    let
        maybePush : (a -> Maybe b) -> a -> Array b -> Array b
        maybePush f mx xs =
            case f mx of
                Just x ->
                    Array.push x xs

                Nothing ->
                    xs
    in
        Array.foldl (maybePush f) Array.empty xs
