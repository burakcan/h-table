module Store.Utils.NormalizePersonList exposing (normalizePersonList)

import Store.Utils.DecodePersonList exposing (RawPersonList(..), RawPerson)
import Store.Types exposing (PersonList(..), Person)
import Maybe
import List
import Array


normalizePersonList : Maybe RawPersonList -> Maybe PersonList
normalizePersonList rawPersonList =
    rawPersonList
        |> Maybe.map
            (\(RawPersonList list) ->
                PersonList <|
                    Array.map
                        (\item ->
                            Person
                                item.data
                                False
                                (normalizePersonList item.kids.has_relatives)
                                (normalizePersonList item.kids.has_phone)
                        )
                        (Array.fromList list)
            )
