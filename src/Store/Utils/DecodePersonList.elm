module Store.Utils.DecodePersonList exposing (decodePersonList, RawPersonList(..), RawPerson)

import Json.Decode as Decode
import Json.Decode
    exposing
        ( Decoder
        , decodeString
        , decodeValue
        , value
        , succeed
        , list
        , (:=)
        , object1
        , object2
        , dict
        , string
        , maybe
        , customDecoder
        )
import Dict exposing (Dict)


type RawPersonList
    = RawPersonList (List RawPerson)


type alias RawKids =
    { has_relatives : Maybe RawPersonList
    , has_phone : Maybe RawPersonList
    }


type alias RawPerson =
    { data : Dict String String
    , kids : RawKids
    }


lazy : (() -> Decoder a) -> Decoder a
lazy getDecoder =
    -- from Json.Decode.Extra
    customDecoder value <|
        \rawValue ->
            decodeValue (getDecoder ()) rawValue


rawPersonDecoder : Decoder RawPerson
rawPersonDecoder =
    object2 RawPerson
        ((:=) "data" <| dict string)
        ((:=) "kids" <|
            object2 RawKids
                (maybe <|
                    (:=) "has_relatives" <|
                        object1 (\rawPersonList -> rawPersonList)
                            ((:=) "records" rawPersonListDecoder)
                )
                (maybe <|
                    (:=) "has_phone" <|
                        object1 (\rawPersonList -> rawPersonList)
                            ((:=) "records" rawPersonListDecoder)
                )
        )


rawPersonListDecoder : Decoder RawPersonList
rawPersonListDecoder =
    lazy
        (\_ ->
            Decode.map (\a -> RawPersonList a) <| list rawPersonDecoder
        )


decodePersonList : String -> Maybe RawPersonList
decodePersonList personList =
    case decodeString (maybe rawPersonListDecoder) personList of
        Err err ->
            Debug.crash err

        Ok result ->
            result
