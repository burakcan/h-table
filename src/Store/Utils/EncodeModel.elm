module Store.Utils.EncodeModel exposing (encodeModel)

import Json.Encode as Encode
import List
import Dict
import Array
import Store.Types exposing (Model, PersonList(..), Person)


encodePersonList : Maybe PersonList -> Encode.Value
encodePersonList personList =
    case personList of
        Nothing ->
            Encode.null

        Just (PersonList personList) ->
            Encode.array <| Array.map encodePerson personList


encodePerson : Person -> Encode.Value
encodePerson person =
    let
        data =
            Encode.object
                (person.data
                    |> Dict.toList
                    |> List.map (\( k, v ) -> ( k, Encode.string v ))
                )
    in
        Encode.object
            [ ( "data", data )
            , ( "expanded", Encode.bool person.expanded )
            , ( "has_relatives", encodePersonList person.has_relatives )
            , ( "has_phone", encodePersonList person.has_phone )
            ]


encodeModel : Model -> String
encodeModel model =
    Encode.encode 0 <|
        Encode.object
            [ ( "personList", encodePersonList model.personList )
            ]
