module Store.Utils.Expand exposing (expand)

import Store.Types exposing (PersonList(..))
import Result
import Array
import String


expand : List String -> PersonList -> PersonList
expand path (PersonList personList) =
    -- Recursively walk in the list by following the path
    -- Btw elm sucks at nesting :)
    Array.indexedMap
        (\i item ->
            let
                pathHead =
                    List.head path

                isMe =
                    Maybe.withDefault False <|
                        Maybe.map
                            (\head ->
                                i == Result.withDefault -i (String.toInt head)
                            )
                            pathHead

                nextItem =
                    if List.length path == 1 && isMe then
                        Just { item | expanded = not item.expanded }
                    else if isMe then
                        Maybe.map
                            (\head ->
                                if head == "has_relatives" then
                                    { item
                                        | has_relatives =
                                            Maybe.map
                                                (expand <| List.drop 2 path)
                                                item.has_relatives
                                    }
                                else if head == "has_phone" then
                                    { item
                                        | has_phone =
                                            Maybe.map
                                                (expand <| List.drop 2 path)
                                                item.has_phone
                                    }
                                else
                                    item
                            )
                        <|
                            List.head (List.drop 1 path)
                    else
                        Just item
            in
                Maybe.withDefault item nextItem
        )
        personList
        |> PersonList
