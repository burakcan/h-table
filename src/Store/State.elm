module Store.State exposing (init, update, subscriptions)

import Store.Border exposing (onDeletePort, onExpandPort, modelDumpPort)
import Store.Types exposing (Flags, Model, Msg(..))
import Store.Utils.EncodeModel exposing (encodeModel)
import Store.Utils.DecodePersonList exposing (decodePersonList)
import Store.Utils.NormalizePersonList exposing (normalizePersonList)
import Store.Utils.Expand exposing (expand)
import Store.Utils.Delete exposing (delete)
import Maybe


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        personList =
            flags.personList
                |> decodePersonList
                |> normalizePersonList

        model =
            { personList = personList
            }
    in
        ( model
        , modelDumpPort <| encodeModel model
        )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ onExpandPort OnExpand
        , onDeletePort OnDelete
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnDelete path ->
            let
                newModel =
                    { model
                        | personList = Maybe.map (delete path) model.personList
                    }
            in
                ( newModel
                , modelDumpPort <| encodeModel newModel
                )

        OnExpand path ->
            let
                newModel =
                    { model
                        | personList = Maybe.map (expand path) model.personList
                    }
            in
                ( newModel
                , modelDumpPort <| encodeModel newModel
                )
